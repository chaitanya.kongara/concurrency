#include <stdio.h> 
#include <pthread.h> 
#include <semaphore.h> 
#include <unistd.h> 
#include<stdlib.h>
#include<time.h>
#include<errno.h>
int k,a,e,c,t1,t2,t_lim;
sem_t acoustic;
sem_t electric;
sem_t pair;
sem_t coor;
pthread_mutex_t mutex[200000];
struct mus{
    char name[1024];
    char I[1];
    int time;
};
struct args{
    char name[1024];
    char I[1];
    int id;
    int idx;
    int time;
};
struct node{
    char name[1024];
    char I[1];
    int idx;
    int paired;
    struct node*next;
};
struct node*head;
int flag[200000]={0};
void merge(struct mus *arr, int l, int mid, int r){
    struct mus left[mid-l+1],right[r-mid];
    for(int i=0;i<(mid-l+1);i++) left[i]=arr[l+i];
    for(int j=0;j<(r-mid);j++) right[j]=arr[mid+j+1];
    int i=0,j=0,k=l;
    while(i<(mid-l+1)&&j<(r-mid)){
        if(left[i].time<=right[j].time){
            arr[k++]=left[i++];
        }
        else arr[k++]=right[j++];
    }
    while(i<(mid-l+1)) arr[k++]=left[i++];
    while(j<(r-mid)) arr[k++]=right[j++];
}

void mergesort(struct mus * brr, int l, int r){
    if(l<r){
        int mid=(l+r)/2;
        mergesort(brr,l,mid);
        mergesort(brr,mid+1,r);
        merge(brr,l,mid,r);
    }
}
void *acoustic_performance(void * var){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += t_lim;
    struct args *per=(struct args*)var;
    int lim=sem_timedwait(&acoustic,&ts);
    //sem_wait(&acoustic);
    //printf("\n%d acoustic\n",per->idx);
    if(lim==-1||errno==ETIMEDOUT){
        pthread_mutex_lock(&mutex[per->idx]);
        if(flag[per->idx]==1) {
        pthread_mutex_unlock(&mutex[per->idx]);
        //sem_post(&acoustic);
        //printf("\n%d acoustic end\n",per->idx);
        return NULL;
        }
        printf("\033[1;31m%s %s left because of impatience.\n\033[0m",per->name,per->I);
        flag[per->idx]=1;
        pthread_mutex_unlock(&mutex[per->idx]);
       // printf("\n%d acoustic end\n",per->idx);
        //sem_post(&acoustic);
        return NULL;
    }
    pthread_mutex_lock(&mutex[per->idx]);
    if(flag[per->idx]==1) {
        pthread_mutex_unlock(&mutex[per->idx]);
        sem_post(&acoustic);
        //printf("\n%d acoustic end\n",per->idx);
        return NULL;
    }
    flag[per->idx]=1;
    pthread_mutex_unlock(&mutex[per->idx]);
    int span=rand()%(t2-t1+1)+t1;
    if(per->I[0]=='s');
    else{
        sem_post(&pair);
        struct node*fake=(struct node*)malloc((sizeof(struct node)));
        fake=head;
        struct node*NODE=(struct node*)malloc((sizeof(struct node)));
        int j=0;
        for(j=0;per->name[j]!='\0';j++) NODE->name[j]=per->name[j];
        NODE->name[j]='\0';
        NODE->I[0]=per->I[0];
        NODE->idx=per->idx;
        NODE->paired=0;
        while(fake->next){
            fake=fake->next;
        }
        fake->next=NODE;
    }
    printf("\033[0;34m%s performing %s at acoustic stage for %d sec\n\033[0m",per->name,per->I,span);
    sleep(span);
    if(per->I[0]=='s');
    else{
        struct node*fake=(struct node*)malloc((sizeof(struct node)));
        fake=head;
        struct node*prev=(struct node*)malloc((sizeof(struct node)));
        prev=head;
        while(fake->idx!=per->idx){
            if(fake->idx==-1){
                fake=fake->next;
                continue;
            }
            prev=fake;
            fake=fake->next;
        }
        if(fake->paired==0) sem_wait(&pair);
        else sleep(2);
        prev->next=fake->next;
    }
    printf("\033[0;31m%s's performance at acoustc stage ended.\n\033[0m",per->name);
    sem_post(&acoustic);
    printf("\033[0;33m%s waiting for t-shirt\n\033[m",per->name);
    sem_wait(&coor);
    printf("\033[0;33m%s collecting t-shirt\n\033[m",per->name);
    sleep(2);
    sem_post(&coor);
    //printf("\n%d acoustic end\n",per->idx);
}
void *electric_performance(void * var){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec+=t_lim;
    struct args *per=(struct args*)var;
    int lim=sem_timedwait(&electric,&ts);
    //sem_wait(&electric);
    //printf("\n%d electric\n",per->idx);
    if(lim==-1||errno==ETIMEDOUT){
        pthread_mutex_lock(&mutex[per->idx]);
        if(flag[per->idx]==1){
        pthread_mutex_unlock(&mutex[per->idx]);
        //sem_post(&electric);
        //printf("\n%d electric end\n",per->idx);
        return NULL;
        } 
        printf("\033[1;31m%s %s left because of impatience.\n\033[0m",per->name,per->I);
        flag[per->idx]=1;
        pthread_mutex_unlock(&mutex[per->idx]);
        //printf("\n%d electric end\n",per->idx);
        //sem_post(&electric);
        return NULL;
    }
    pthread_mutex_lock(&mutex[per->idx]);
    if(flag[per->idx]==1){
        pthread_mutex_unlock(&mutex[per->idx]);
        sem_post(&electric);
        //printf("\n%d electric end\n",per->idx);
        return NULL;
    } 
    flag[per->idx]=1;
    pthread_mutex_unlock(&mutex[per->idx]);
    if(per->I[0]=='s');
    else{
        sem_post(&pair);
        struct node*fake=(struct node*)malloc(sizeof(struct node));
        fake=head;
        struct node*NODE=(struct node*)malloc(sizeof(struct node));
        int j=0;
        for(j=0;per->name[j]!='\0';j++) NODE->name[j]=per->name[j];
        NODE->name[j]='\0';
        NODE->I[0]=per->I[0];
        NODE->idx=per->idx;
        NODE->paired=0;
        while(fake->next){
            fake=fake->next;
        }
        fake->next=NODE;
    }
    int span=rand()%(t2-t1+1)+t1;
    printf("\033[0;34m%s performing %s at electric stage for %d sec\n\033[0m",per->name,per->I,span);
    sleep(span);
    if(per->I[0]=='s');
    else {
        struct node*fake=(struct node*)malloc(sizeof(struct node));
        fake=head;
        struct node*prev=(struct node*)malloc(sizeof(struct node));
        prev=head;
        while(fake->idx!=per->idx){
            if(fake->idx==-1){
                fake=fake->next;
                continue;
            }
            prev=fake;
            fake=fake->next;
        }
        if(fake->paired==0) sem_wait(&pair);
        else sleep(2);
        prev->next=fake->next;
    }
    printf("\033[0;31m%s's performance at electric stage ended.\n\033[0m",per->name);
    sem_post(&electric);
    printf("\033[0;33m%s waiting for t-shirt\n\033[m",per->name);
    sem_wait(&coor);
    printf("\033[0;33m%s collecting t-shirt\n\033[m",per->name);
    sleep(2);
    sem_post(&coor);
    //printf("\n%d electric end\n",per->idx);
}
void *paired_performance(void * var){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_nsec += t_lim;
    struct args *per=(struct args*)var;
    int lim=sem_timedwait(&pair,&ts);
    //printf("\n%d paired\n",per->idx);
    if(lim==-1||errno==ETIMEDOUT){
        pthread_mutex_lock(&mutex[per->idx]);
        if(flag[per->idx]==1){
        pthread_mutex_unlock(&mutex[per->idx]);
        //sem_post(&pair);
        //printf("\n%d paired end\n",per->idx);
        return NULL;
        }
        printf("\033[1;31m%s %s left because of impatience.\n\033[0m",per->name,per->I);
        flag[per->idx]=1;
        pthread_mutex_unlock(&mutex[per->idx]);
        //printf("\n%d paired end\n",per->idx);
        //sem_post(&pair);
        return NULL;
    }
    pthread_mutex_lock(&mutex[per->idx]);
    if(flag[per->idx]==1){
        pthread_mutex_unlock(&mutex[per->idx]);
        sem_post(&pair);
        //printf("\n%d paired end\n",per->idx);
        return NULL;
    } 
    flag[per->idx]=1;
    pthread_mutex_unlock(&mutex[per->idx]);
    struct node*fake=(struct node*)malloc((sizeof(struct node)));
    fake=head;
    while(fake->paired!=0){
        fake=fake->next;
    }
    fake->paired=1;
    printf("\033[0;34m%s joined %s's performance, got extended by 2 sec\n\033[0m",per->name,fake->name);
    //printf("\n%d paired end\n",per->idx);
}
int main(){
    struct node*Node=(struct node*)malloc(sizeof(struct node));
    head=(struct node*)malloc(sizeof(struct node));
    head=Node;
    Node->name[0]='a';
    Node->I[0]='x';
    Node->paired=-1;
    Node->idx=-1;
    Node->next=NULL;
    srand(time(0));
    scanf("%d%d%d%d%d%d%d",&k,&a,&e,&c,&t1,&t2,&t_lim);
    for(int i=0;i<k;i++) pthread_mutex_init(&mutex[i],NULL);
    sem_init(&acoustic,0,a);
    sem_init(&electric,0,e);
    sem_init(&pair,0,0);
    sem_init(&coor,0,c);
    struct mus *inp=(struct mus*)malloc(k*sizeof(struct mus));
    for(int i=0;i<k;i++){
        scanf("%s%s%d",inp[i].name,inp[i].I,&inp[i].time);
    }
    mergesort(inp,0,k-1);
    int t_prev=0;
    pthread_t t[k][3];
    struct args *var=(struct args*)malloc(k*sizeof(struct args));
    //for(int i=0;i<k;i++){
        //printf("%s %s %d\n",inp[i].name,inp[i].I,inp[i].time);}
    for(int i=0;i<k;i++){
        flag[i]=0;
        //printf("%s %s %d\n",inp[i].name,inp[i].I,inp[i].time);
        //var->name=inp[i].name;
        int j;
        for(j=0;inp[i].name[j]!='\0';j++) var[i].name[j]=inp[i].name[j];
        var[i].name[j]='\0';
        //fake->name[j]='\0';
        var[i].I[0]=inp[i].I[0];
        //fake->I[0]=inp[i].I[0];
        var[i].time=inp[i].time;
        var[i].idx=i;
        //removed the sleep statements and the next line from ifs, brought it up 
        sleep(inp[i].time-t_prev);
        t_prev=inp[i].time;
        printf("\033[0;32m%s %s arrived\n\033[0m",var[i].name,var[i].I);
        if(inp[i].I[0]=='v'){
            var[i].id=0;
            pthread_create(&t[i][0],NULL,acoustic_performance,(void*)&var[i]);
        }
        if(inp[i].I[0]=='b'){
            var[i].id=1;
            pthread_create(&t[i][1],NULL,electric_performance,(void *)&var[i]);
        }
        if(inp[i].I[0]=='p'||inp[i].I[0]=='g'){
            var[i].id=2;
            pthread_create(&t[i][0],NULL,acoustic_performance,(void *)&var[i]);
            pthread_create(&t[i][1],NULL,electric_performance,(void*)&var[i]);
        }
        if(inp[i].I[0]=='s'){
            var[i].id=3;
            pthread_create(&t[i][1],NULL,electric_performance,(void*)&var[i]);
            pthread_create(&t[i][0],NULL,acoustic_performance,(void*)&var[i]);
            pthread_create(&t[i][2],NULL,paired_performance,(void*)&var[i]);
        }
    }
    for(int i=0;i<k;i++) {if(var[i].id==2){pthread_join(t[i][0],NULL);pthread_join(t[i][1],NULL);}else if(var[i].id==3){pthread_join(t[i][0],NULL);pthread_join(t[i][1],NULL);pthread_join(t[i][2],NULL);} else pthread_join(t[i][var[i].id],NULL);}
    printf("Finished");
    return 0;
}