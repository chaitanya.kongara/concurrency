# Concurrency

Handled a real life scenarios that involves concurrency, in C language using multi threading and thus semaphores and mutex locks. (For more info related to the scenario head over to [MusicEvent.pdf](./MusicEvent.pdf) )

## Run the Simulation

1. Clone this directory and `cd` into it.
2. Run the executable using the command `./sim`.
3. Provide the Event details as input (as mentioned in the [MusicEvent.pdf](./MusicEvent.pdf))
